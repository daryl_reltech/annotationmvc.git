package com.daryl.struts.utils;

import java.lang.reflect.Field;

import javax.servlet.http.HttpServletRequest;

import com.daryl.struts.form.ActionForm;

public class FillForm {
	
	public FillForm(){
		
	}
	
	public static ActionForm fill(String formClass,HttpServletRequest request){
		
		ActionForm form = null;
		try{
			Class clazz = Class.forName(formClass);
			form = (ActionForm) clazz.newInstance();
			Field[] fieldArr = clazz.getDeclaredFields();
			for(Field f : fieldArr){
				f.setAccessible(true);
				f.set(form, request.getParameter(f.getName()));
				f.setAccessible(false);
			}			
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("严重： form装载失败！。。。。。。");
		}
		return form;
	}
}
