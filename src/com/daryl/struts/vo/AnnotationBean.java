package com.daryl.struts.vo;


public class AnnotationBean {
	
	public AnnotationBean(){
			
	}
	

	private String path;
	private String actionClass;
	private String formClass;
		
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getActionClass() {
		return actionClass;
	}
	public void setActionClass(String actionClass) {
		this.actionClass = actionClass;
	}
	public String getFormClass() {
		return formClass;
	}
	public void setFormClass(String formClass) {
		this.formClass = formClass;
	}
	

}
