package com.daryl.struts.aciton;

import javax.servlet.http.HttpServletRequest;

import com.daryl.struts.form.ActionForm;

public interface Action {
	String execute(HttpServletRequest request, ActionForm form );
}
