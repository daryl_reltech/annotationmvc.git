package com.daryl.struts.aciton;

import java.io.IOException;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.daryl.struts.form.ActionForm;
import com.daryl.struts.utils.FillForm;
import com.daryl.struts.utils.PathUtil;
import com.daryl.struts.vo.AnnotationBean;

public class ActionServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		//获得请求路径
		String path = PathUtil.getPath(request.getServletPath());
		//获得注解信息
		Map<String,AnnotationBean> map = (Map<String, AnnotationBean>) this.getServletContext().getAttribute("pathMapping");
		//根据路径获得对应的注解信息
		AnnotationBean bean = map.get(path);
		String formClass = bean.getFormClass();
		ActionForm form = FillForm.fill(formClass, request);
		String actionClass = bean.getActionClass();
		Action action = null;
		String url = "";
		try{
			Class clazz = Class.forName(actionClass);
			action = (Action) clazz.newInstance();
			url = action.execute(request, form);
		}catch(Exception e){
			System.out.println("严重：控制器异常。。。。");
		}
		RequestDispatcher dis = request.getRequestDispatcher(url);
		dis.forward(request, response);
	}
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		this.doGet(request, response);
	}
	
	
	

}
