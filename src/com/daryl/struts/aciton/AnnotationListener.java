package com.daryl.struts.aciton;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.daryl.struts.utils.PackageScanUtil;
import com.daryl.struts.vo.AnnotationBean;

public class AnnotationListener implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		ServletContext context = arg0.getServletContext();
		String packageScanPath = context.getInitParameter("package-scan-path");
		String[] packageScanpaths = packageScanPath.split(",");
		Set<Class<?>> classeSet = new LinkedHashSet<Class<?>>();
		for (String packageScanpath : packageScanpaths) {
			classeSet.addAll(PackageScanUtil.getClasses(packageScanpath));
		}
		Map<String,AnnotationBean> map = new HashMap<String,AnnotationBean>();
		for (Class<?> clazz : classeSet) {
			PackageScanUtil.controllerHanler(map,clazz);
		}
		context.setAttribute("pathMapping", map);
		System.out.println("系统已经加载完成");
		
	}

}
