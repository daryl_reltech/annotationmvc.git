package com.daryl.mytest.action;

import javax.servlet.http.HttpServletRequest;

import com.daryl.mytest.service.LoginService;
import com.daryl.mytest.service.impl.LoginServiceImpl;
import com.daryl.mytest.vo.Account;
import com.daryl.struts.aciton.Action;
import com.daryl.struts.annotation.Form;
import com.daryl.struts.annotation.RequestMapping;
import com.daryl.struts.form.ActionForm;

@RequestMapping("/login")
@Form("com.daryl.mytest.vo.Account")
public class LoginAction implements Action {

	@Override
	public String execute(HttpServletRequest request, ActionForm form) {
		Account account = (Account) form;
		LoginService loginService = new LoginServiceImpl();
		boolean flag = loginService.isLogin(account);
		request.setAttribute("account", account);
		if(flag){
			return "/webpage/login/Success.jsp";
		}		
		return "/webpage/login/Fail.jsp";
	}

		
}
