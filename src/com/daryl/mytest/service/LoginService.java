package com.daryl.mytest.service;

import com.daryl.mytest.vo.Account;

public interface LoginService {
	
	public boolean isLogin(Account account);
}
